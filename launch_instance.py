#!/usr/bin/python

from string import Template

import boto.ec2

PUPPET_SOURCE = 'https://bitbucket.org/diegovbarros/play-ec2-puppetboot'

def get_script(filename='user-data-script.sh'):
    template = open(filename).read()
    return Template(template).substitute(
        puppet_source=PUPPET_SOURCE,
    )

    def get_script(filename='user-data-script.sh'):
    template = open(filename).read()
    return Template(template).substitute(
        puppet_source=PUPPET_SOURCE,
        deploy_keys_known_hosts=open('deploy_keys/known_hosts').read().strip(),
        deploy_keys_deploy_public=open('deploy_keys/id_rsa.pub').read().strip(),
        deploy_keys_deploy_private=open('deploy_keys/id_rsa').read().strip(),
    )

def launch():
    connection = boto.ec2.connect_to_region('us-east-1')
    return connection.run_instances(
        image_id = 'ami-6ba27502',  # us-east-1 oneiric i386 ebs 20120108
        instance_type = 't1.micro',
        key_name = 'awskey',
        security_groups = ['default'],
        user_data=get_script(),
    )

if __name__ == '__main__':
    launch()
