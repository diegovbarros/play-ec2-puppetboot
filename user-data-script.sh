#!/bin/sh
set -e -x

apt-get --yes --quiet update
apt-get --yes --quiet install git puppet-common

#
# Set up ssh keys to enable git read access.
#

cat <<EOF >>/root/.ssh/known_hosts
$deploy_keys_known_hosts
EOF

cat <<EOF >/root/.ssh/id_rsa.pub
$deploy_keys_deploy_public
EOF

cat <<EOF >/root/.ssh/id_rsa
$deploy_keys_deploy_private
EOF

chmod 600 /root/.ssh/id_rsa

#
# Fetch puppet configuration using git.
#

mv /etc/puppet /etc/puppet.orig
git clone $puppet_source /etc/puppet

#
# Run puppet.
#

puppet apply /etc/puppet/manifests/init.pp
