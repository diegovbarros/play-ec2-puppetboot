# README #

Play EC2 puppetboot

### What is this repository for? ###

With this bootstrap you can launch Amazon's Ec2 instances with Play Framework + JDK pre-installed.

It was inspired by this [article](http://blog.codento.com/2012/02/hello-ec2-part-1-bootstrapping-instances-with-cloud-init-git-and-puppet/), which uses cloud-init, git, and [puppet](http://puppetlabs.com/) for bootstraping.

### What this bootstrap installs? ###

Your Ec2 instance will be up and running the following stuff:

* JDK 1.7 (JAVA_HOME, JAVA, JAVAC, all you need...)
* Play 2.3.3

### How do i change the Play Version? ###

This bootstrap only supports Play 2.1.x and 2.2.x.

Check the [Wiki](https://bitbucket.org/dbalduini/play-ec2-puppetboot/wiki/Home) for guiding in how to change Play's version.

### Installation ###
You can either:

1. Use an [user-data](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html) file. [(link here)](https://bitbucket.org/dbalduini/play-ec2-puppetboot/wiki/UserData)
2. Use a python script. [(link here)](https://bitbucket.org/dbalduini/play-ec2-puppetboot/wiki/launch_instance)

Check the [Wiki](https://bitbucket.org/dbalduini/play-ec2-puppetboot/wiki/Home) for more details about installation.